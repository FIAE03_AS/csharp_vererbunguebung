﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vererbung___Übung
{
    class Duck : IDuck, IHerbivore
    {
        public string GetTypeOFHerbivore() => "Primary Consumer";

        public String[] PrefersTypeOfPlant() => new String[] { "Algae"};

        public double GetPercentageOfPlantDiet() => 95.0;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vererbung___Übung
{
    public class Tiger : Carnivore, ITiger
    {
        public override int MammaryGlands { get; set; } = 6;
        public int GetNumberofStripes { get; set; } = 42;

        public override bool GivesBirthtoLivingYoung()
        {
            return true;
        }

        public override string MakesNoise()
        {
            return "Miarowr";
        }

        public override bool IsPackAnimal()
        {
            return false;
        }

        public override string GetTypeOfCarnnivore()
        {
            return "Hypercanivore";
        }

        public override String[] GetPreferredTypeOfPrey()
        {
            return new String[] {"Deer", "Boar"};
        }

        public override double GetPercentageOfMeatDiet()
        {
            return 70.5;
        }
    }
}

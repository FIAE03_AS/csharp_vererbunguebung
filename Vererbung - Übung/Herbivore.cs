﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vererbung___Übung
{
    public abstract class Herbivore : Mammal, IHerbivore
    {
        public abstract string GetTypeOFHerbivore();
        public abstract String[] PrefersTypeOfPlant();
        public abstract double GetPercentageOfPlantDiet();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vererbung___Übung
{
    public abstract class Mammal : IMammal
    {
        public abstract int MammaryGlands { get; set; }
        public abstract bool GivesBirthtoLivingYoung();
        public abstract string MakesNoise();
        public abstract bool IsPackAnimal();
    }
}
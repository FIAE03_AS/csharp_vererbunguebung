﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vererbung___Übung
{
    public abstract class Carnivore : Mammal, ICarnivore
    {
        public abstract String GetTypeOfCarnnivore();
        public abstract String[] GetPreferredTypeOfPrey();
        public abstract double GetPercentageOfMeatDiet();
    }
}

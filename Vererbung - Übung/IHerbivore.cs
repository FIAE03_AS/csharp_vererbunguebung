﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vererbung___Übung
{
    interface IHerbivore
    {
        string GetTypeOFHerbivore();
        String[] PrefersTypeOfPlant();
        double GetPercentageOfPlantDiet();
    }
}

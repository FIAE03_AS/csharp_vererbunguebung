﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vererbung___Übung
{
    public interface IMammal
    {
        int MammaryGlands { get; set; }

        bool GivesBirthtoLivingYoung();
        string MakesNoise();
        bool IsPackAnimal();
    }
}